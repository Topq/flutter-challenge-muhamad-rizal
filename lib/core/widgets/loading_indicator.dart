import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({Key? key, this.color, this.size}) : super(key: key);
  final Color? color;
  final double? size;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox.square(
        dimension: size ?? 20,
        child: LoadingAnimationWidget.discreteCircle(
          size: size ?? 20,
          color: color ?? Colors.deepPurple,
          secondRingColor: Colors.deepPurple.shade400,
          thirdRingColor: Colors.deepPurple.shade900,
          // strokeWidth: 2,
        ),
      ),
    );
  }
}
