import 'package:ffcapps/home/domain/entities/category_entity.dart';

abstract class CategoryRepository {
  Future<List<CategoryEntity>> listCategory();
}
