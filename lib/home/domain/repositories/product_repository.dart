import 'package:ffcapps/home/domain/entities/product_entity.dart';

abstract class ProductRepository {
  Future<List<ProductEntity>> listProduct();
  Future<ProductEntity> category();
  Future<List<ProductEntity>> searchProduct(String title);
}
