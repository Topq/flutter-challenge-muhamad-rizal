import 'package:ffcapps/home/domain/entities/category_entity.dart';
import 'package:ffcapps/home/domain/repositories/category_repository.dart';

class CategoryUseCase {
  final CategoryRepository categoryRepository;

  CategoryUseCase(this.categoryRepository);

  Future<List<CategoryEntity>> category() async {
    return categoryRepository.listCategory();
  }
}
