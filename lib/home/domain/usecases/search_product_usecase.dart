import 'package:ffcapps/home/domain/entities/product_entity.dart';
import 'package:ffcapps/home/domain/repositories/product_repository.dart';

class SearchProductUseCase {
  final ProductRepository productRepository;

  SearchProductUseCase(this.productRepository);

  Future<List<ProductEntity>> searchProduct(String title) async {
    return productRepository.searchProduct(title);
  }
}
