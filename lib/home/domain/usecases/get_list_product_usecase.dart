import 'package:ffcapps/home/domain/entities/product_entity.dart';
import 'package:ffcapps/home/domain/repositories/product_repository.dart';

class GetListProductUseCase {
  final ProductRepository productRepository;

  GetListProductUseCase(this.productRepository);

  Future<List<ProductEntity>> listProduct() async {
    return productRepository.listProduct();
  }
}
