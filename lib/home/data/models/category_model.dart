import 'package:ffcapps/home/domain/entities/category_entity.dart';

class CategoryModel extends CategoryEntity {
  final String name;

  CategoryModel({
    required this.name,
  }) : super(
          name: '',
        );

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
      };
}
