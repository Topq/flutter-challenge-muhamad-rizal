import 'dart:convert';
import 'dart:developer';

import 'package:ffcapps/home/data/models/category_model.dart';
import 'package:ffcapps/home/domain/entities/category_entity.dart';
import 'package:ffcapps/home/domain/repositories/category_repository.dart';
import 'package:http/http.dart' as http;

class CategoryRepositoryImpl extends CategoryRepository {
  final String baseUrl = 'https://dummyjson.com';

  @override
  Future<List<CategoryEntity>> listCategory() async {
    final response = await http.get(
      Uri.parse('$baseUrl/products/categories'),
    );
    log(response.body);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final List<CategoryModel> listCategory = [];

      for (var e in data) {
        // log(e);
        listCategory.add(CategoryModel(name: e));
      }
      log(listCategory[0].name.toString());
      return listCategory;
    } else {
      throw Exception('Get Data failed');
    }
  }
}
