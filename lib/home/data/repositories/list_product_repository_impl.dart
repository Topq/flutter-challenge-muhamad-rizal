import 'dart:convert';
import 'dart:developer';

import 'package:ffcapps/home/data/models/product_model.dart';
import 'package:ffcapps/home/domain/entities/product_entity.dart';
import 'package:ffcapps/home/domain/repositories/product_repository.dart';
import 'package:http/http.dart' as http;

class ProductRepositoryImpl extends ProductRepository {
  final String baseUrl = 'https://dummyjson.com';

  @override
  Future<ProductEntity> category() async {
    final response = await http.get(
      Uri.parse('$baseUrl/products/categories'),
    );
    log(response.body);
    if (response.statusCode == 200) {
      final Map<String, dynamic> json = jsonDecode(response.body);
      final product = ProductModel.fromJson(json);
      return product;
    } else {
      throw Exception('Get Data failed');
    }
  }

  @override
  Future<List<ProductEntity>> listProduct() async {
    final response = await http.get(
      Uri.parse('$baseUrl/products'),
    );
    log(response.body);
    if (response.statusCode == 200) {
      final Map<String, dynamic> json = jsonDecode(response.body);
      final List<ProductModel> listProduct = [];

      for (var e in json['products']) {
        final product = ProductModel.fromJson(e);
        listProduct.add(product);
      }
      return listProduct;
    } else {
      throw Exception('Get Data failed');
    }
  }

  @override
  Future<List<ProductEntity>> searchProduct(String title) async {
    final response = await http.get(
      Uri.parse('$baseUrl/products/search?q=$title'),
    );
    log(response.body);
    if (response.statusCode == 200) {
      final Map<String, dynamic> json = jsonDecode(response.body);
      final List<ProductEntity> listProduct = [];

      for (var e in json['results']) {
        final product = ProductModel.fromJson(e);
        listProduct.add(product);
      }
      return listProduct;
    } else {
      throw Exception('Get Data failed');
    }
  }
}
