import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ffcapps/home/domain/entities/category_entity.dart';
import 'package:ffcapps/home/domain/repositories/category_repository.dart';
import 'package:ffcapps/home/domain/usecases/get_category_usecase.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  final CategoryUseCase categoryUseCase;
  final CategoryRepository categoryRepository;

  CategoryBloc(this.categoryUseCase, this.categoryRepository)
      : super(CategoryInitial()) {
    on<GetListCategory>((event, emit) async {
      log(event.toString());
      emit(const CategoryLoading());
      try {
        final category = await categoryUseCase.category();
        emit(CategorySuccess(category));
      } catch (error) {
        emit(CategoryFailure(error.toString()));
      }
    });
  }
}
