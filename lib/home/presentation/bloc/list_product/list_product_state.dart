part of 'list_product_bloc.dart';

sealed class ListProductState extends Equatable {
  const ListProductState();

  @override
  List<Object> get props => [];
}

class ListProductInitial extends ListProductState {}

class ListProductLoading extends ListProductState {
  const ListProductLoading();
}

class ListProductSuccess extends ListProductState {
  final List<ProductEntity> product;

  const ListProductSuccess(this.product);

  @override
  List<Object> get props => [product];
}

class ListProductFailure extends ListProductState {
  final String error;

  const ListProductFailure(this.error);

  @override
  List<Object> get props => [error];
}

class ListProductFiltered extends ListProductState {
  final List<ProductEntity> filteredProducts;

  const ListProductFiltered(this.filteredProducts);

  @override
  List<Object> get props => [filteredProducts];
}
