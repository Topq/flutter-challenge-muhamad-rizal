import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ffcapps/home/domain/entities/product_entity.dart';
import 'package:ffcapps/home/domain/repositories/product_repository.dart';
import 'package:ffcapps/home/domain/usecases/get_list_product_usecase.dart';

part 'list_product_event.dart';
part 'list_product_state.dart';

class ListProductBloc extends Bloc<ListProductEvent, ListProductState> {
  final GetListProductUseCase getListProductUseCase;
  final ProductRepository productRepository;

  ListProductBloc(this.getListProductUseCase, this.productRepository)
      : super(ListProductInitial()) {
    on<GetListProduct>((event, emit) async {
      log(event.toString());
      emit(const ListProductLoading());
      try {
        final product = await getListProductUseCase.listProduct();
        log(product[0].title);
        emit(ListProductSuccess(product));
      } catch (error) {
        emit(ListProductFailure(error.toString()));
      }
    });
    on<FilterByCategory>((event, emit) async {
      log(event.toString());
      emit(const ListProductLoading());
      try {
        final product = await getListProductUseCase.listProduct();
        final filteredProducts =
            product.where((p) => p.category == event.category).toList();
        emit(ListProductFiltered(filteredProducts));
      } catch (error) {
        emit(ListProductFailure(error.toString()));
      }
    });
  }
}
