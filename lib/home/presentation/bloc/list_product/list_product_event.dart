part of 'list_product_bloc.dart';

sealed class ListProductEvent extends Equatable {
  const ListProductEvent();

  @override
  List<Object> get props => [];
}

class GetListProduct extends ListProductEvent {}

class FilterByCategory extends ListProductEvent {
  final String
      category; // Replace with the appropriate data type for your category

  const FilterByCategory(this.category);

  @override
  List<Object> get props => [category];
}
