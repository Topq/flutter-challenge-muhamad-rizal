import 'package:ffcapps/core/widgets/loading_indicator.dart';
import 'package:ffcapps/home/domain/entities/product_entity.dart';
import 'package:ffcapps/home/presentation/bloc/category_product/category_bloc.dart';
import 'package:ffcapps/home/presentation/bloc/list_product/list_product_bloc.dart';
import 'package:ffcapps/home/presentation/view/components/list_item.dart';
import 'package:ffcapps/home/presentation/view/detail_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class ListProductScreen extends StatelessWidget {
  const ListProductScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _buildAppbar(context),
        body: Column(
          children: [
            _buildCategory(),
            _buildBody(),
          ],
        ));
  }

  _buildAppbar(BuildContext context) {
    return AppBar(
      title: InkWell(
          onTap: () =>
              BlocProvider.of<ListProductBloc>(context).add(GetListProduct()),
          child: Row(
            children: [
              Image.asset(
                'assets/logo.png',
                width: 50,
              ),
              const SizedBox(
                width: 10,
              ),
              const Text(
                'Product',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: Colors.deepPurple),
                textAlign: TextAlign.center,
              ),
            ],
          )),
      actions: [
        // GestureDetector(
        //   onTap: () => _onShowSavedArticlesViewTapped(context),
        //   child: const Padding(
        //     padding: EdgeInsets.symmetric(horizontal: 14),
        //     child: Icon(Icons.bookmark, color: Colors.black),
        //   ),
        // ),
      ],
    );
  }

  _buildCategory() {
    return BlocBuilder<CategoryBloc, CategoryState>(builder: (_, state) {
      if (state is CategoryLoading) {
        return const Center(child: LoadingIndicator());
      }
      if (state is CategoryFailure) {
        return const Center(child: Icon(Icons.refresh));
      }
      if (state is CategorySuccess) {
        return Container(
            margin: const EdgeInsets.all(10),
            height: 50,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: state.category.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () => BlocProvider.of<ListProductBloc>(context)
                        .add(FilterByCategory(state.category[index].name)),
                    child: Container(
                      // width: 100,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.deepOrange.shade500,
                      ),
                      margin: const EdgeInsets.all(5),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        state.category[index].name,
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                }));
      }
      return const SizedBox(
        child: Text(''),
      );
    });
  }

  _buildBody() {
    return BlocBuilder<ListProductBloc, ListProductState>(
      builder: (_, state) {
        if (state is ListProductLoading) {
          return const Center(child: LoadingIndicator());
        }
        if (state is ListProductFailure) {
          return const Center(child: Icon(Icons.refresh));
        }
        if (state is ListProductFiltered) {
          // Display the filtered product list here
          return Expanded(
              child: GridView.builder(
            padding: const EdgeInsets.all(5),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, // Number of columns in the grid
              crossAxisSpacing: 15.0, // Spacing between columns
              mainAxisSpacing: 15.0, // Spacing between rows
            ),
            itemBuilder: (context, index) {
              return ListItem(
                product: state.filteredProducts[index],
              );
            },
            itemCount: state.filteredProducts.length,
          ));
        }
        if (state is ListProductSuccess) {
          return Expanded(
            child: GridView.builder(
              padding: const EdgeInsets.all(5),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, // Number of columns in the grid
                crossAxisSpacing: 15.0, // Spacing between columns
                mainAxisSpacing: 15.0, // Spacing between rows
              ),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () => onProductPressed(context, state.product[index]),
                  child: ListItem(
                    product: state.product[index],
                  ),
                );
              },
              itemCount: state.product.length,
            ),
          );
        }
        return const SizedBox(
          child: Text('anjay'),
        );
      },
    );
  }

  void onProductPressed(BuildContext context, ProductEntity product) {
    PersistentNavBarNavigator.pushNewScreen(context,
        withNavBar: false, screen: DetailProduct(product: product));
  }

  void _onShowSavedArticlesViewTapped(BuildContext context) {
    Navigator.pushNamed(context, '/SavedArticles');
  }
}
