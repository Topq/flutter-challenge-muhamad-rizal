import 'package:ffcapps/auth/domain/entities/user_entity.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';

class LoginUseCase {
  final AuthRepository authRepository;

  LoginUseCase(this.authRepository);

  Future<UserEntity> login(String username, String password) async {
    return authRepository.login(username, password);
  }
}
