class UserEntity {
  final int id;
  final String username;
  final String email;
  final String firstName;
  final String lastName;
  final String gender;
  final String image;
  final String token;

  UserEntity(
      {required this.id,
      required this.username,
      required this.token,
      required this.email,
      required this.firstName,
      required this.lastName,
      required this.gender,
      required this.image});
}
