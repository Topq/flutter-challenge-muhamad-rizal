import 'dart:developer';

import 'package:ffcapps/auth/data/repositories/auth_repository_impl.dart';
import 'package:ffcapps/auth/domain/entities/user_entity.dart';
import 'package:ffcapps/auth/domain/usecases/login_usecase.dart';
import 'package:ffcapps/auth/presentation/bloc/login_bloc.dart';
import 'package:ffcapps/home/presentation/view/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(40),
        color: Colors.white,
        child: const Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Don't have an account?",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.start,
            ),
            SizedBox(
              width: 4,
            ),
            Text(
              "Sign Up",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.deepOrange),
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ),
      body: BlocProvider(
        create: (context) =>
            LoginBloc(LoginUseCase(AuthRepositoryImpl()), AuthRepositoryImpl()),
        child: Column(
          children: [
            Row(
              children: [
                Image.asset(
                  'assets/header-login.png',
                  width: 130,
                ),
                const SizedBox(
                  width: 10,
                ),
                Image.asset(
                  'assets/logo.png',
                  width: 110,
                )
              ],
            ),
            LoginForm(),
          ],
        ),
      ),
    );
  }
}

class LoginForm extends StatelessWidget {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    Future<void> successLoginInfo(UserEntity user) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Login Success'),
            icon: const Icon(
              Icons.check_circle_rounded,
              color: Colors.green,
              size: 100,
            ),
            content: const SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  // Text('Login Success'),
                  Text('Next you will go to the home page'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Next'),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) => HomeScreen(
                        user: user,
                      ),
                    ),
                  );
                },
              ),
            ],
          );
        },
      );
    }

    Future<void> failedLoginInfo() async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Login Failed'),
            icon: const Icon(
              Icons.close_outlined,
              color: Colors.red,
              size: 100,
            ),
            content: const SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('User ID or Password is wrong'),
                  // Text('Would you like to approve of this message?'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Back'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccess) {
          // Navigasi ke halaman berikutnya setelah login berhasil
          log('login berhasil');
          successLoginInfo(state.user);
        } else if (state is LoginFailure) {
          failedLoginInfo();
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Center(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 80,
                  ),
                  const Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w800,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  const Text(
                    'Please sign in to continue',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: usernameController,
                    decoration: InputDecoration(
                        labelText: 'User ID',
                        hintText: 'User ID',
                        hintStyle: TextStyle(
                            color: Colors.grey.withOpacity(0.5),
                            fontSize: 12,
                            fontStyle: FontStyle.italic)),
                  ),
                  TextField(
                    controller: passwordController,
                    decoration: InputDecoration(
                        labelText: 'Password',
                        hintText: 'Password',
                        hintStyle: TextStyle(
                            color: Colors.grey.withOpacity(0.5),
                            fontSize: 12,
                            fontStyle: FontStyle.italic)),
                    obscureText: true,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: SizedBox(
                      height: 40,
                      width: 140,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.deepPurple),
                        onPressed: () {
                          // final username = 'kminchelle';
                          // final password = '0lelplR';

                          final username = usernameController.text;
                          final password = passwordController.text;
                          context.read<LoginBloc>().add(LoginButtonPressed(
                              username: username,
                              password:
                                  password)); // Buat instance dari event LoginButtonPressed
                        },
                        child: const Text(
                          'LOGIN',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
