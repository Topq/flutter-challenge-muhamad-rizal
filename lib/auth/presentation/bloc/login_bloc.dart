import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ffcapps/auth/domain/entities/user_entity.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';
import 'package:ffcapps/auth/domain/usecases/login_usecase.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUseCase loginUseCase;
  final AuthRepository authRepository;
  LoginBloc(this.loginUseCase, this.authRepository) : super(LoginInitial()) {
    // Daftarkan event handler di sini
    on<LoginButtonPressed>((event, emit) async {
      emit(LoginLoading());
      try {
        log(event.username);
        log(event.password);
        final user = await loginUseCase.login(event.username, event.password);
        emit(LoginSuccess(user));
      } catch (error) {
        emit(LoginFailure(error.toString()));
      }
    });
  }
}
