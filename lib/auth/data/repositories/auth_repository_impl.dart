import 'dart:convert';
import 'dart:developer';

import 'package:ffcapps/auth/data/models/user_model.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';
import 'package:http/http.dart' as http;

class AuthRepositoryImpl extends AuthRepository {
  final String baseUrl = 'https://dummyjson.com';

  @override
  Future<UserModel> login(String username, String password) async {
    final response = await http.post(
      Uri.parse('$baseUrl/auth/login'),
      body: {
        'username': username,
        'password': password,
      },
    );
    log(response.body);
    if (response.statusCode == 200) {
      final Map<String, dynamic> json = jsonDecode(response.body);
      final user = UserModel.fromJson(json);
      return user;
    } else {
      throw Exception('Login failed');
    }
  }
}
