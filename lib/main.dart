import 'package:ffcapps/auth/presentation/views/login_screen.dart';
import 'package:ffcapps/home/data/repositories/list_category_repository_impl.dart';
import 'package:ffcapps/home/data/repositories/list_product_repository_impl.dart';
import 'package:ffcapps/home/domain/usecases/get_category_usecase.dart';
import 'package:ffcapps/home/domain/usecases/get_list_product_usecase.dart';
import 'package:ffcapps/home/presentation/bloc/category_product/category_bloc.dart';
import 'package:ffcapps/home/presentation/bloc/list_product/list_product_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<ListProductBloc>(
            create: (BuildContext context) => ListProductBloc(
              GetListProductUseCase(ProductRepositoryImpl()),
              ProductRepositoryImpl(),
            )..add(GetListProduct()),
          ),
          BlocProvider<CategoryBloc>(
            create: (BuildContext context) => CategoryBloc(
                CategoryUseCase(CategoryRepositoryImpl()),
                CategoryRepositoryImpl())
              ..add(GetListCategory()),
          ),
          // BlocProvider<BlocC>(
          //   create: (BuildContext context) => BlocC(),
          // ),
        ],
        child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            home: const SplashScreen()
            // const MyHomePage(title: 'Flutter Demo Home Page'),
            ));
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const LoginScreen(),
        ),
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Align(
          alignment: Alignment.topCenter,
          child: Image.asset(
            'assets/header-splash.png',
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/logo.png',
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Image.asset(
            'assets/footer-splash.png',
          ),
        )
      ]),
    );
  }
}
