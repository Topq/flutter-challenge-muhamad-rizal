import 'package:flutter_test/flutter_test.dart';
import 'package:ffcapps/auth/domain/entities/user_entity.dart';

void main() {
  group('UserEntity', () {
    test('should create a UserEntity instance', () {
      // Arrange
      final user = UserEntity(
        id: 1,
        username: 'test_user',
        token: 'test_token',
        email: 'test@example.com',
        firstName: 'John',
        lastName: 'Doe',
        gender: 'Male',
        image: 'profile.jpg',
      );

      // Act & Assert
      expect(user.id, 1);
      expect(user.username, 'test_user');
      expect(user.token, 'test_token');
      expect(user.email, 'test@example.com');
      expect(user.firstName, 'John');
      expect(user.lastName, 'Doe');
      expect(user.gender, 'Male');
      expect(user.image, 'profile.jpg');
    });

    // Add more tests as needed for specific scenarios or edge cases
  });
}
