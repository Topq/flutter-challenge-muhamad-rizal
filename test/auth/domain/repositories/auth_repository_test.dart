import 'package:ffcapps/auth/data/repositories/auth_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ffcapps/auth/domain/entities/user_entity.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';

void main() {
  group('AuthRepository', () {
    late AuthRepository authRepository;

    setUp(() {
      // Initialize AuthRepository before each test.
      authRepository = AuthRepositoryImpl(); // Replace with the actual implementation class.
    });

    test('login should return a UserEntity', () async {
      // Arrange
      final username = 'kminchelle';
      final password = '0lelplR';

      // Act
      final result = await authRepository.login(username, password);

      // Assert
      expect(result, isA<UserEntity>());
    });
  });
}
