import 'package:ffcapps/auth/domain/usecases/login_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:ffcapps/auth/domain/entities/user_entity.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';

import 'login_usecase_test.mocks.dart';

// Mock AuthRepository to use in testing
class MockAuthRepository extends Mock implements AuthRepository {}
@GenerateNiceMocks([
  MockSpec<MockAuthRepository>(),
])

void main() {
  group('LoginUseCase', () {
    test('should return a UserEntity when login is successful', () async {
      // Arrange
      final mockAuthRepository = MockMockAuthRepository();
      final loginUseCase = LoginUseCase(mockAuthRepository);
      final username = 'kminchelle';
      final password = '0lelplR';

      // Mock the behavior of the authRepository's login method
      when(mockAuthRepository.login(username, password))
          .thenAnswer((_) async => UserEntity(id: 1, username: 'testUser', token: '', email: '', firstName: '', lastName: '', gender: '', image: ''));

      // Act
      final result = await loginUseCase.login(username, password);

      // Assert
      expect(result, isA<UserEntity>());
      expect(result.username, 'testUser');
    });

    test('should throw an exception when login fails', () async {
      // Arrange
      final mockAuthRepository = MockMockAuthRepository();
      final loginUseCase = LoginUseCase(mockAuthRepository);
      final username = 'kminchelle';
      final password = '0lelplR';

      // Mock the behavior of the authRepository's login method to throw an exception
      when(mockAuthRepository.login(username, password)).thenThrow(Exception('Login failed'));

      // Act
      // Assert
      expect(
        () => loginUseCase.login(username, password),
        throwsA(isA<Exception>()),
      );
    });
  });
}
