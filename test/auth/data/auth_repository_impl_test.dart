import 'dart:convert';

import 'package:ffcapps/auth/data/models/user_model.dart';
import 'package:ffcapps/auth/domain/repositories/auth_repository.dart';
import 'package:ffcapps/auth/data/repositories/auth_repository_impl.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import 'auth_repository_impl_test.mocks.dart';

class MockHttpClient extends Mock implements http.Client {}
@GenerateNiceMocks([
  MockSpec<MockHttpClient>(),
])

void main() {
  group('AuthRepositoryImpl', () {
    late AuthRepository authRepository;
    late MockMockHttpClient mockHttpClient;

    setUp(() {
      mockHttpClient = MockMockHttpClient();
      authRepository = AuthRepositoryImpl();
    });

    test('login - successful response', () async {
      // Arrange
      final username = 'kminchelle';
      final password = '0lelplR';

      // Mock successful HTTP response
      when(mockHttpClient.post(
        Uri.parse('https://dummyjson.com/auth/login/'),
        body: anyNamed('body'),
      )).thenAnswer((_) async =>
          http.Response(jsonEncode({'userId': 15, 'username': 'kminchelle'}), 200));

      // Act
      final result = await authRepository.login(username, password);

      // Assert
      expect(result, isA<UserModel>());
      expect(result.id, 15);
      expect(result.username, 'kminchelle');
    });

    test('login - failed response', () async {
      // Arrange
      final username = 'test_user';
      final password = 'test_password';

      // Mock failed HTTP response
      when(mockHttpClient.post(
        Uri.parse('https://dummyjson.com/auth/login'),
        body: anyNamed('body'),
      )).thenAnswer((_) async => http.Response('Unauthorized', 401));

      // Act and Assert
      expect(
        () async => await authRepository.login(username, password),
        throwsA(predicate((dynamic e) => e is Exception && e.toString().contains('Login failed'))),
      );
    });
  });
}
