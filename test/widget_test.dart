import 'package:flutter_test/flutter_test.dart';
import 'package:ffcapps/auth/data/models/user_model.dart';

void main() {
  group('UserModel', () {
    test('fromJson should create a UserModel instance from JSON', () {
      // Arrange
      Map<String, dynamic> json = {
        'id': 1,
        'username': 'john_doe',
        'email': 'john.doe@example.com',
        'firstName': 'John',
        'lastName': 'Doe',
        'gender': 'Male',
        'image': 'avatar.jpg',
        'token': 'token123',
      };

      // Act
      UserModel userModel = UserModel.fromJson(json);

      // Assert
      expect(userModel.id, equals(1));
      expect(userModel.username, equals('john_doe'));
      expect(userModel.email, equals('john.doe@example.com'));
      expect(userModel.firstName, equals('John'));
      expect(userModel.lastName, equals('Doe'));
      expect(userModel.gender, equals('Male'));
      expect(userModel.image, equals('avatar.jpg'));
      expect(userModel.token, equals('token123'));
    });

    test('fromJson should handle missing fields in JSON', () {
      // Arrange
      Map<String, dynamic> json = {
        'id': 1,
        'username': 'john_doe',
        'email': '',
        'firstName': '',
        'lastName': '',
        'gender': '',
        'image': '',
        'token': '',
        // Other fields are missing
      };

      // Act
      UserModel userModel = UserModel.fromJson(json);

      // Assert
      expect(userModel.id, equals(1));
      expect(userModel.username, equals('john_doe'));
      // Ensure that other fields have their default values
      expect(userModel.email, equals(''));
      expect(userModel.firstName, equals(''));
      expect(userModel.lastName, equals(''));
      expect(userModel.gender, equals(''));
      expect(userModel.image, equals(''));
      expect(userModel.token, equals(''));
    });
  });
}
